Author: Michael Hale  
Lab 2 Assignment  
CSCI 344 Purewal

Using HTML, CSS, jQuery and cTwitter, build an application that does the followi ng:

1) It tracks some technology-related term(s) that are being commonly used.

2) It displays at most 10 tweets at a time (after the 10 most recent tweets are displayed, the earliest tweets start to disappear to make room for the new tweets)

3) It must use jQuery's fadeOut/fadeIn or slideDown/slideUp methods in some way.
