$(document).ready(function () {
  var twitter = new ctwitter.CTwitter();

  twitter.stream("statuses/filter", { 
    lang: "en", 
    track: ["laravel", "symfony", "python"] 
  }, function (stream) {
    stream.on("data", function (tweet) {
      $(".tweets").prepend("<p class='tweet'>" + tweet.text + "</p>");
      $(".tweet").fadeIn(800);

      if ($(".tweet").size() >= 10) {
        $(".tweet:last").fadeOut(400, function() {
          $(this).remove();
        });
      }
    });
  });
});